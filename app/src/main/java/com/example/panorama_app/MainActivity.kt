package com.example.panorama_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var allPanorama=ArrayList<Panoroma>() //data class tipinde listimizi tanimlaadik

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dataSource()
        //recyclerView ozelligi layout manager tanımlamak gerekiyor



        var myAdapter=PanaromaAdapter(allPanorama)
        recyclerViewPanoroma.adapter=myAdapter


        var linearLayoutManager=LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false) //true yaparsan tersten listelenir
        recyclerViewPanoroma.layoutManager=linearLayoutManager

    }

    //menulerle calistigimizda kullanmamiz gereken method
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.main_menu,menu) // olusturudumuz main_menu menu  olarak parametre gelen menu koy

        return super.onCreateOptionsMenu(menu)
    }

    //MENUdeki ogelere tiklnaninca ne yapmasi lazim bu methodda
    override fun onOptionsItemSelected(item: MenuItem): Boolean {


        var id=item.itemId

        when(id){

            //recyclerViewPanoroma adapter layot tipini degisitiroyor grid mi lineerlayout mu gibi
            R.id.menuLinearViewHorizontal -> {
                var linearLayoutManager=LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
                recyclerViewPanoroma.layoutManager=linearLayoutManager //yeni atama yapiyor yukrda linear layout yapmistik
            }


            R.id.menuLinearViewVertical -> {

                var menuLinearViewVertical=LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
                recyclerViewPanoroma.layoutManager=menuLinearViewVertical

            }


            R.id.menuGrid-> {
                var menuGrid=GridLayoutManager(this,2)
                recyclerViewPanoroma.layoutManager=menuGrid
            }


            R.id.menuStaggeredHorizontal -> {
                var menuStaggeredHorizontal=StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.HORIZONTAL)
                recyclerViewPanoroma.layoutManager=menuStaggeredHorizontal
            }

            R.id.menuStaggeredVertical-> { //icerikler
                var menuStaggeredVertical=StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL)
                recyclerViewPanoroma.layoutManager=menuStaggeredVertical
            }

        }

        return super.onOptionsItemSelected(item)
    }


    fun dataSource():ArrayList<Panoroma>{

        var allImg= arrayOf(R.drawable.thumb_1_0,R.drawable.thumb_1_1,R.drawable.thumb_1_2,R.drawable.thumb_1_3,R.drawable.thumb_1_4,
            R.drawable.thumb_1_5,R.drawable.thumb_1_6,R.drawable.thumb_1_7,R.drawable.thumb_1_8,R.drawable.thumb_1_9,

            R.drawable.thumb_2_0,R.drawable.thumb_2_1,R.drawable.thumb_2_2,R.drawable.thumb_2_3,R.drawable.thumb_2_4,
            R.drawable.thumb_2_5,R.drawable.thumb_2_6,R.drawable.thumb_2_7,R.drawable.thumb_2_8,R.drawable.thumb_2_9,

            R.drawable.thumb_3_0,R.drawable.thumb_3_1,R.drawable.thumb_3_2,R.drawable.thumb_3_3,R.drawable.thumb_3_4,
            R.drawable.thumb_3_5,R.drawable.thumb_3_6,R.drawable.thumb_3_7,R.drawable.thumb_3_8,R.drawable.thumb_3_9,

            R.drawable.thumb_4_0,R.drawable.thumb_4_1,R.drawable.thumb_4_2,R.drawable.thumb_4_3,R.drawable.thumb_4_4,
            R.drawable.thumb_4_5,R.drawable.thumb_4_6,R.drawable.thumb_4_7,R.drawable.thumb_4_8,R.drawable.thumb_4_9,

            R.drawable.thumb_5_0,R.drawable.thumb_5_1,R.drawable.thumb_5_2,R.drawable.thumb_5_3,R.drawable.thumb_5_4,
            R.drawable.thumb_5_5,R.drawable.thumb_5_6,R.drawable.thumb_5_7,R.drawable.thumb_5_8,R.drawable.thumb_5_9,

            R.drawable.thumb_6_0,R.drawable.thumb_6_1,R.drawable.thumb_6_2,R.drawable.thumb_6_3,R.drawable.thumb_6_4,
            R.drawable.thumb_6_5,R.drawable.thumb_6_6,R.drawable.thumb_6_7,R.drawable.thumb_6_8,R.drawable.thumb_6_9,

            R.drawable.thumb_7_0,R.drawable.thumb_7_1,R.drawable.thumb_7_2,R.drawable.thumb_7_3,R.drawable.thumb_7_4


        )

        for(i in 0..allImg.size-1){
            var addPanoroma=Panoroma("Manzara ${i} ","explanation:  ${i}",allImg[i])
            allPanorama.add(addPanoroma)

        }

        return  allPanorama
    }


}
