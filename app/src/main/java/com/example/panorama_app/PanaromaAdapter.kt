package com.example.panorama_app

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.only_line_panorama.view.*
import java.util.zip.Inflater

class PanaromaAdapter(allDataPanoroma:ArrayList<Panoroma> ): RecyclerView.Adapter<PanaromaAdapter.PanoromaViewHolder>() { //adapter olusturuldunda constracter veriyi atayalim(biz boyle yapiyoruz)

    var panoroma=allDataPanoroma


    override fun getItemCount(): Int { // RecyclerView calistiginda ilk baktigi yer burasi(gosterilecek oge sayisi)

//        Log.e("Recyclerview","getItemCount tetiklendi")

        return  panoroma.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PanoromaViewHolder { // hazirladigimiz xml dinamik olarak(kotlin/java donsturuldugu kisim)

        //parent burda recycleview temsil ediyor(onun icine yaptigimiz icin)
        //inflater calistiracagimiz kisim(xml java donusuturyoruz)

        var inflater=LayoutInflater.from(parent.context) //context erismek icin yap mainActivity parametre  gonderecek yada view grouptan gelen parent elde edebiliriz contexti

        var onlyLinePanoroma=inflater.inflate(R.layout.only_line_panorama,parent,false)


//        Log.e("Recyclerview","onCreateViewHolder tetiklendi")


        return  PanoromaViewHolder(onlyLinePanoroma)  // only_line_panorama.xml var onlyLinePanoroma de( PanoromaViewHolder(itemView: View)) itemView bizim onlyLinePanoromadir
    }


    override fun onBindViewHolder(holder: PanoromaViewHolder, position: Int) { //secilen cardview icine veri atma burda oalcak(ayrıstırılmıs elemanlar cardlara atayacagiz)

        //TODO biz burda set kullandik islemde text= yapiliyor

        var tempData=panoroma[position] //her bir item teker teker tempData atiliyor
        holder.setData(tempData,position)

        //asagidada yapilablir ama bunlar bir funtion toplama daha iyi bolmek (daha kolay bolunurs)

//        holder.panoromaTitle.setText(panoroma[position].title)
//        holder.txtPanExplanation.setText(panoroma[position].explanation)
//
//        holder.imgPanorama.setImageResource(panoroma[position].img)

//        Log.e("Recyclerview","onBindViewHolder tetiklendi")
    }



  inner  class  PanoromaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) { //Cardview deki elemanları bulma islemi burda oalacak(icindeki textviewlere erisme islemi burda oalcak)

       // PanoromaViewHolder bir ic sinif distaki siniflari erismek istiyoruzz onun icin ic sinafa inner koyuyoruz
        //textview mi imgview mi yoksa farki viewlermi burda atiyoruz

        var onlyPanoroma=itemView as CardView  //typcasting yap yoksa text viewlere erisimeyiouz

        var panoromaTitle=onlyPanoroma.txtPanTitle
        var txtPanExplanation=onlyPanoroma.txtPanExplanation

        var imgPanorama=onlyPanoroma.imgPanorama

        var btnCopy=onlyPanoroma.imgCopy
        var btnDelete=onlyPanoroma.imgDelete
        //butonlari tetiklemem gerek init uygun oldugunu dusunduk

        init {
//            Log.e("Recyclerview","PanoromaViewHolder class  tetiklendi")
                //islemler icin mutlaka position degerine ihtiyacimiz var
//            btnCopy.setOnClickListener {  }
//            btnDelete.setOnClickListener {  }
        }

        fun setData(currentPanaroma:Panoroma,position: Int){ //

                                    //o anki manzara(item imn manzarası)
            panoromaTitle.setText(currentPanaroma.title)
            txtPanExplanation.setText(currentPanaroma.explanation)

            imgPanorama.setImageResource(currentPanaroma.img)


            btnCopy.setOnClickListener {

                //ilgili posiyon degerini  ekle

                panoroma.add(position,currentPanaroma)
                //not ekledigini adaptere bildirmen gerek notifyItemInserted ile yapiyoruz
                 notifyItemInserted(position)
                //adaptera silme yada eklemede eleman sayisini  bildirmek gerekiyor yoksa expansion bound hayasi alabilirsiniz
                notifyItemRangeChanged(position,panoroma.size)
            }
            btnDelete.setOnClickListener {

                //ilgili posiyon degerini sil
                panoroma.removeAt(position)
                //not sildigini adaptere bildirmen gerek notifyItemRemoved ile yapiyoruz
                notifyItemRemoved(position)
                //adaptera silme yada eklemede eleman sayisini  bildirmek gerekiyor yoksa expansion bound hayasi alabilirsiniz
                notifyItemRangeChanged(position,panoroma.size)


            }
        }

    }


}